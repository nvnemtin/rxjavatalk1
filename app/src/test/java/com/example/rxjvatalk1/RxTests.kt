package com.example.rxjvatalk1

import io.reactivex.subjects.PublishSubject
import org.junit.Assert.assertTrue
import org.junit.Test
import java.util.concurrent.CountDownLatch
import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.thread
import android.os.AsyncTask.execute
import java.util.concurrent.Executors


class RxTests {

    @Test
    fun `break PublishSubject`() {
        val numberOfThreads = 10
        repeat(100000) {
            println("Iteration = $it")

            val publishSubject = PublishSubject.create<Int>()
            val latch = CountDownLatch(numberOfThreads)
            val threads = mutableListOf<Thread>()
            val actuallyReceived = AtomicInteger()
            val counter = AtomicInteger()

            publishSubject
                    .doOnNext { counter.incrementAndGet() }
                    .doOnNext { counter.decrementAndGet() }
                    .filter { counter.get() != 0 }
                    .subscribe { actuallyReceived.incrementAndGet() }

            (0..numberOfThreads).forEach {
                threads += thread(start = false) {
                    publishSubject.onNext(it)
                    latch.countDown()
                }
            }
            threads.forEach { it.start() }

            latch.await()
            val got = actuallyReceived.get()
            assertTrue("got $got", actuallyReceived.get() == 0)
        }
    }

//    @Test
//    fun `break take(n) v2`() {
//        val numberOfThreads = 10
//        repeat(100000) {
//            println("Iteration = $it")
//
//            val actuallyReceived = AtomicInteger()
//            Observable.create<Int> { emitter ->
//                val latch = CountDownLatch(numberOfThreads)
//                val threads = mutableListOf<Thread>()
//                (0..numberOfThreads).forEach {
//                    threads += thread(start = false) {
//                        emitter.onNext(it)
//                        latch.countDown()
//                    }
//                }
//                threads.forEach { thread -> thread.start() }
//                latch.await()
//                emitter.onComplete()
//            }.serialize()
//                    .take(3)
//                    .blockingSubscribe { actuallyReceived.incrementAndGet() }
//
//            val got = actuallyReceived.get()
//            assertTrue("got $got", actuallyReceived.get() == 3)
//        }
//    }

//    @Test
//    fun `break scan`() {
//        val numberOfThreads = 10
//        repeat(100000) {
//            println("Iteration = $it")
//
//            val threads = mutableListOf<Thread>()
//            var last = 0
//            Observable.defer<Int> {
//                ObservableSource<Int> { observer ->
//                    val latch = CountDownLatch(numberOfThreads)
//                    (0 until numberOfThreads).forEach {
//                        threads += thread(start = false) {
//                            observer.onNext(it)
//                            latch.countDown()
//                        }
//                    }
//                    threads.forEach { thread -> thread.start() }
//                    latch.await()
//                    observer.onComplete()
//                }
//            }.serialize()
//                    .scan(0) { old, new ->
//                        old + new
//                    }
//                    .blockingSubscribe {
//                        last = it
//                    }
//            assertTrue("got $last", last == 45)
//        }
//    }

//    @Test
//    fun `break distinct()`() {
//        val numberOfThreads = 10
//        repeat(100000) {
//            println("Iteration = $it")
//
//            val actuallyReceived = AtomicInteger()
//            val source = ObservableSource<Int> { observer ->
//                val threads = mutableListOf<Thread>()
//                val latch = CountDownLatch(numberOfThreads)
//                (0 until numberOfThreads).forEach { threadNum ->
//                    threads.add(
//                            thread(start = false) {
//                                repeat(100) {
//                                    observer.onNext(threadNum)
//                                }
//                                latch.countDown()
//                            }
//                    )
//                }
//                threads.forEach { thread -> thread.start() }
//                latch.await()
//                observer.onComplete()
//            }
//            Observable.defer<Int> { source }
//                    .serialize()
//                    .distinct()
//                    .blockingSubscribe {
//                        actuallyReceived.incrementAndGet()
//                    }
//            val got = actuallyReceived.get()
//            assertTrue("got $got", actuallyReceived.get() == 10)
//        }
//    }
}
